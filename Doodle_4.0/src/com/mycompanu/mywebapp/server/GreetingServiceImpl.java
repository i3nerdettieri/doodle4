package com.mycompanu.mywebapp.server;

import java.io.File;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ConcurrentNavigableMap;
import org.mapdb.*;

import com.mycompanu.mywebapp.client.Evento;
import com.mycompanu.mywebapp.client.GreetingService;
import com.mycompanu.mywebapp.client.Utente;
import com.mycompanu.mywebapp.shared.FieldVerifier;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;



@SuppressWarnings("serial")
public class GreetingServiceImpl extends RemoteServiceServlet implements GreetingService
{
	private Scanner scanner;
    DB db = DBMaker.newFileDB(new File("doodle")).make();
	Map<Integer,Evento> mapEvento = db.getTreeMap("Eventi");



	public String inserisciUtente (String input) throws IllegalArgumentException
	{
		DB db = DBMaker.newFileDB(new File("doodle")).closeOnJvmShutdown().make();
		
		/*
		 * Creo le mappe di cui avr� bisogno:
		 * UserInfo - infromazioni dell utente
		 * UserId - <IdUtente><Username>
		 */
		
	    ConcurrentNavigableMap<String,String> listaUtenti = db.getTreeMap("ListaUtenti");
	    
	    /*
	     * leggiamo i dati inseriti in input
	     */
	    
	    scanner = new Scanner(input);
		Scanner scan = scanner.useDelimiter("\\s*\\,\\s*");
	    String nome = scan.next();
	    String username = scan.next();
	    String email = scan.next();
	    String password = scan.next();
	    
	    /*
	     * prepariamo i dati che andremo ad inviare
	     */
	    
	    System.out.println(listaUtenti.containsKey(username));
	    System.out.println(listaUtenti.containsValue(email));
	    System.out.println(listaUtenti.containsKey(username)||listaUtenti.containsValue(email));
	    if(!(listaUtenti.containsKey(username)||listaUtenti.containsValue(email)))
	    {
	    	System.out.println("********** Utente non presente **********");

		    ConcurrentNavigableMap<String,Utente> mapUtenti = db.getTreeMap("Users");
		    ConcurrentNavigableMap<String,String> mapPass = db.getTreeMap("UsersPass");
		    
	    	Utente user = new Utente(nome,username,email, password);
	    	
	    	listaUtenti.put(username,email);
	    	mapUtenti.put(username,user);
	    	mapPass.put(username,password);
	    	
	    	System.out.println("stampiamo la mappa degli utenti");
		    System.out.println(mapUtenti);
	    	System.out.println("stampiamo la lista di utenti - email");
		    System.out.println(listaUtenti);
		    System.out.println("lista utente - password");
		    System.out.println(mapPass);
			    
	    }else{
	    	username =username+"X";
	    	System.out.println("porca vacca funziona ed esiste gi�");
	    }
	    
	    /*
	     * chiudiamo tutti gli oggetti di lettura aperti
	     */
	    
	    db.commit();
	    scan.close();
	    db.close();
	    
		return username;
		
	}
	
	

	

  /**
   * Escape an html string. Escaping data received from the client helps to
   * prevent cross-site script vulnerabilities.
   * 
   * @param html the html string to escape
   * @return the escaped string
   */
	private String escapeHtml(String html)
	{
		if (html == null)
		{
			return null;
    	}
		return html.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;");
	}



	public String controlloUsername(String name) throws IllegalArgumentException
	{
		System.out.println("stringa in arrivo " + name);
		scanner = new Scanner(name);
		Scanner scan = scanner.useDelimiter("\\s*\\,\\s*");
		String username = scan.next();
		String password = scan.next();
		DB db = DBMaker.newFileDB(new File("doodle")).make();
		
		
		/* 
		 *  creo --> mapId - idUtente - Username
		 */
		
		ConcurrentNavigableMap<String,String> mapId = db.getTreeMap("UsersPass");
		
		System.out.println(mapId);

		if(mapId.containsKey(username)&&mapId.containsValue(password)){
			System.out.println("trovato");
			return username;
		}else{
			System.out.println("non trovato");
		}
		return username+"X";
	}



	public String inserisciEvento(String evento)
	{
		System.out.println(evento);
		scanner = new Scanner(evento);
		Scanner scan = scanner.useDelimiter("\\s*\\,\\s*");
		DB dbcd = DBMaker.newFileDB(new File("doodleEv")).closeOnJvmShutdown().make();
	    //DB dbcd = DBMaker.newFileDB(new File("doodle")).make();
		ConcurrentNavigableMap<Integer,Evento> mapEvento = dbcd.getTreeMap("eventiP");
		ConcurrentNavigableMap<String,String> mapUtenteEv = dbcd.getTreeMap("utenteEvento");

		String nome = scan.next();
		String luogo = scan.next();
		String descrizione = scan.next();
		String username = scan.next();
		
		System.out.println(nome);
		System.out.println(luogo);
		System.out.println(descrizione);
		System.out.println(username);
		String s[];
		s = new String[] {"anesario","mattia","filippo"};
		System.out.println("errore1");
		int idEvento = CreaId.creaID("utenteEvento", 2);
		Evento ev = new Evento(nome,luogo,descrizione,s);
		System.out.println("errore2");
		/*ev.setCreatore("cojones");
		String[] ins = new String[10];
		ins[0]="xxx";
		ins[1]="eee";
		ev.setDate(ins);
		ev.setCommenti(ins);*/
		
		
		//System.out.println(ev.getCreatore() + " sto stampando " + ev.getLuogo() + ev.getNomeEvento());
		
		String all;
		System.out.println("id - "+idEvento);
		System.out.println("evento - " + ev);
		mapEvento.put(idEvento,ev);
		System.out.println("errore3");
		dbcd.commit();
		if(mapUtenteEv.containsKey(username))
		{
			System.out.println(mapUtenteEv);
			System.out.println("ringhi");
			all = mapUtenteEv.get(username).toString();
			System.out.println("critical");
			System.out.println("all - " +all);
			all = all + " , " + idEvento;
			mapUtenteEv.put(username,all);
		}else{
			mapUtenteEv.put(username,idEvento+"");
			System.out.println(mapUtenteEv);
		}
		System.out.println("errore4");
		dbcd.commit();
		
		System.out.println("controllo altra mappa");
		System.out.println(mapEvento);
		System.out.println(mapUtenteEv);

		dbcd.close();
		return "ciao";
	}
}